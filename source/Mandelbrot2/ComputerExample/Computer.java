
public abstract class Computer {
private String os;

private void setOs(String value) {
   this.os = value;
}

private String getOs() {
   return this.os;
}

private RAM ram;

public void setRam(RAM value) {
   this.ram = value;
}

public RAM getRam() {
   return this.ram;
}


private Motherboard motherboard;

public void setMotherboard(Motherboard value) {
   this.motherboard = value;
}

public Motherboard getMotherboard() {
   return this.motherboard;
}


private CPU cpu;

public void setCpu(CPU value) {
   this.cpu = value;
}

public CPU getCpu() {
   return this.cpu;
}

public void powerOn() {
   // TODO implement this operation
   throw new UnsupportedOperationException("not implemented");
}

public Computer () {
   // TODO implement this operation
   throw new UnsupportedOperationException("not implemented");
}

}
