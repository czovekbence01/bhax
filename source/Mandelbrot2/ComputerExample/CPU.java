
public class CPU {
private String manufacturer;

private void setManufacturer(String value) {
   this.manufacturer = value;
}

private String getManufacturer() {
   return this.manufacturer;
}

private double clockFrequency;

private void setClockFrequency(double value) {
   this.clockFrequency = value;
}

private double getClockFrequency() {
   return this.clockFrequency;
}

private int cores;

private void setCores(int value) {
   this.cores = value;
}

private int getCores() {
   return this.cores;
}

private String socket;

private void setSocket(String value) {
   this.socket = value;
}

private String getSocket() {
   return this.socket;
}

public CPU () {
   // TODO implement this operation
   throw new UnsupportedOperationException("not implemented");
}

}
