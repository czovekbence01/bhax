package com.bence;

public class Main {

    public static void main(String[] args) {

        Oktato prog2Okt = new Oktato("12345","John", "Doe");
        Oktato habiOkt = new Oktato("12346","Jane", "Doe");
        Targy prog2 = new Targy("INBPM0315-17","Magas szintű programozási nyelvek 2","Java, C++",
                "nappali",18,6,false,prog2Okt);
        Targy habi = new Targy("INBPM9935-17","Haladó adatbázis ismeretek","SQL","nappali",
                18,3,false,habiOkt);
        Hallgato hallgato1 = new Hallgato("QWERT","Czövek","Bence","PTI","nappali");

        prog2.jelentkezes(hallgato1);
        habi.jelentkezes(hallgato1);
        hallgato1.printFelvettTargyak();
        System.out.println("A félévre felvett kreditek száma: " + hallgato1.getFelvettKreditek());

        for(int i = 0; i < 20;i++){
            Hallgato hallg = new Hallgato(String.valueOf(i),"Hallgató",String.valueOf(i),"PTI","nappali");
            prog2.jelentkezes(hallg);
        }

        System.out.println(prog2);
    }

}
