package com.bence;

public class Oktato {

    private String neptunKod;
    private String vezeteknev;
    private String keresztnev;
    private Targy[] targyak;
    private int targyakSzama = 0;

    public Oktato(String neptunKod, String vezeteknev, String keresztnev) {
        this.neptunKod = neptunKod;
        this.vezeteknev = vezeteknev;
        this.keresztnev = keresztnev;
        targyak = new Targy[10];
    }

    public void addTargy(Targy targy){
        targyak[targyakSzama] = targy;
        targyakSzama++;
    }

    public String getNev(){
        return this.vezeteknev+" "+this.keresztnev;
    }
}
