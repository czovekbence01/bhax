package com.bence;

public class Targy {

    private String targykod;
    private String nev;
    private String leiras;
    private String tagozat;
    private int hallgatoLimit;
    private int kredit;
    private boolean vizsga;
    private Hallgato[] hallgatok;
    private Oktato oktato;
    private int hallgatokSzama = 0;


    public Targy(String targykod, String nev, String leiras, String tagozat, int hallgatoLimit, int kredit, boolean vizsga,Oktato oktato) {
        this.targykod = targykod;
        this.nev = nev;
        this.leiras = leiras;
        this.tagozat = tagozat;
        this.hallgatoLimit = hallgatoLimit;
        this.kredit = kredit;
        this.vizsga = vizsga;
        this.hallgatok = new Hallgato[hallgatoLimit];
        this.oktato = oktato;
        oktato.addTargy(this);
    }

    public String getTargykod() {
        return targykod;
    }

    public String getNev() {
        return nev;
    }


    public int getKredit() {
        return kredit;
    }


    public Oktato getOktato() {
        return oktato;
    }

    @Override
    public String toString() {
        return this.nev+"\n"+this.leiras+"\nFő / Limit: "+hallgatokSzama + " / " + hallgatoLimit;
    }

    public void jelentkezes(Hallgato hallgato){
        if(hallgatokSzama < hallgatoLimit && hallgato.getTagozat().toLowerCase().equals(tagozat.toLowerCase())){
            hallgatok[hallgatokSzama] = hallgato;
            hallgatokSzama++;
            hallgato.addTargy(this);
            System.out.println(hallgato.getNev()+" sikeresen felvette a(z) "+nev+" tárgyat");

        }else
        {
            System.out.println("Tárgyfelvétel sikertelen");
        }
    }

}
