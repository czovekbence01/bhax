package com.bence;

public class Hallgato {

    private String neptunKod;
    private String vezeteknev;
    private String keresztnev;
    private String szak;
    private String tagozat;
    private int felvettKreditek;
    private Targy[] felvettTargyak;
    private int felvettTargyakSzama = 0;

    public Hallgato(String neptunKod, String vezeteknev, String keresztnev, String szak, String tagozat) {
        this.neptunKod = neptunKod;
        this.vezeteknev = vezeteknev;
        this.keresztnev = keresztnev;
        this.szak = szak;
        this.tagozat = tagozat;
        this.felvettTargyak = new Targy[10];
    }

    public String getNev(){
        return this.vezeteknev+" "+this.keresztnev;
    }


    public String getTagozat() {
        return tagozat;
    }

    public void printFelvettTargyak(){
        System.out.println(getNev()+ " felvett tárgyai:");
        for(int i = 0; i<felvettTargyakSzama; i++){
            System.out.println(felvettTargyak[i].getTargykod()+"      "+felvettTargyak[i].getNev()+" Oktató: "+ felvettTargyak[i].getOktato().getNev());
        }
    }

    public int getFelvettKreditek() {
        for (int i=0; i<felvettTargyakSzama;i++){
            felvettKreditek+=felvettTargyak[i].getKredit();
        }
        return felvettKreditek;
    }

    public void addTargy(Targy targy){
        felvettTargyak[felvettTargyakSzama] = targy;
        felvettTargyakSzama++;
    }


}
