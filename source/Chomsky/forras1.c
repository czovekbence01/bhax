#include <signal.h>
#include <stdio.h>

void jelkezelo(int signum){
    printf("Jel: %d",signum);
}

int main(){

    if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
        signal(SIGINT, jelkezelo);
        

    return 0;
}