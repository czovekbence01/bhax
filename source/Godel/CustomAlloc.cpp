#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <set>

using namespace std;


template<typename T>
class CustomAlloc{
public:
    using size_type = size_t;
    using pointer = T*;
    using const_pointer = const T*;
    using value_type = T;

    pointer allocate(int n){

        cout << "Allocating " << n << " objects of " << n*sizeof(value_type) << endl;

        return reinterpret_cast<pointer>(new char[n*sizeof(value_type)]);
    }

    void deallocate(pointer p ,size_type n){
        cout << "Deallocating " << n << " objects of " << n*sizeof(value_type) << endl;

        delete [] reinterpret_cast<char *>(p);
    }
};

int main(){

    vector<int, CustomAlloc<int>> v;

    v.push_back(27);
    v.push_back(28);
    v.push_back(29);
    
}