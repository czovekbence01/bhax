import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Adott egy “legacy” kód mely tartalmaz anonymus interface implementációkat, ciklusokat és feltételes kifejezések.
 * Ebben a feladatban ezt a “legacy” kódot szeretnénk átírni lambda kifejezések segítségével (metódus referencia használata előnyt jelent).
 *
 * A program jelenlegi kimenete:
 *  Runnable!
 *  Calculation result: 9
 *  Result numbers:
 *  1
 *  9
 *  25
 *  Formatted numbers: 1925
 */
public class LegacyRefactoring {

    public void legacy() {
        Runnable runnable = () -> {
            System.out.println("Runnable!");
        };
        runnable.run();

        Calculator calculator = (number) -> {
            return number*number;
        };

        Integer result = calculator.calculate(3);
        System.out.println("Calculation result: " + result);

        List<Integer> inputNumbers = Arrays.asList(1, null, 3, null, 5);
        List<Integer> resultNumbers = inputNumbers.stream().filter(Objects::nonNull)
                .map(calculator::calculate)
                .collect(Collectors.toList());

        Consumer<Integer> method = System.out::println;
        System.out.println("Result numbers: ");
        resultNumbers.forEach(method);

        Formatter formatter = (numbers) -> {
            StringBuilder sb = new StringBuilder();
            numbers.stream().forEach(n -> sb.append(String.valueOf(n)));
            return sb.toString();
        };

        System.out.println("Formatted numbers: " + formatter.format(resultNumbers));
    }

}