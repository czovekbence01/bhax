import java.util.Arrays;
import java.util.stream.IntStream;

public class Matrix {
    double[][] matrix;
    int rows;
    int cols;

    public Matrix(double[][] matrix){
        this.matrix = matrix;
        this.rows = matrix.length;
        this.cols = matrix[0].length;
    }

    public double[][] getMatrix(){
        return matrix;
    }
    public int getRows(){
        return rows;
    }
    public int getCols(){
        return cols;
    }

    public Matrix multiply(Matrix m){
        double[][] result = Arrays.stream(this.matrix)
                .map(r -> IntStream.range(0, m.getCols())
                        .mapToDouble(i -> IntStream.range(0, m.getRows())
                                .mapToDouble(j -> r[j] * m.getMatrix()[j][i]).sum())
                        .toArray())
                .toArray(double[][]::new);
        return new Matrix(result);
    }

    public void printMatrix(){
        System.out.println(Arrays.deepToString(matrix));
    }
}