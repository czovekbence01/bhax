public class Main {

    public static void main(String[] args) {
        Matrix m1 = new Matrix(new double[][]{{1, 2}, {3, 4}, {5, 6}});
        Matrix m2 = new Matrix(new double[][]{{4, 3}, {2, 1}});

        Matrix result = m1.multiply(m2);
        result.printMatrix();
    }
}
