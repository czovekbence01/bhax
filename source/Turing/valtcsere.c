#include <stdio.h>

void variableswap(int a, int b);
void sumdivswap(int a, int b);
void xorswap(int a,int b);
int main()
{
    int var1=12, var2=7;
    printf("Változók eredeti értéke:\na=%d\nb=%d\n",var1,var2);
    variableswap(var1,var2);
    sumdivswap(var1,var2);
    xorswap(var1,var2);
}
void variableswap(int a, int b)
{
    int temp;
    temp=a;
    a=b;
    b=temp;
    printf("Csere segédváltozóval:\na=%d\nb=%d\n",a,b);
}
void sumdivswap(int a, int b)
{
    a=a+b;
    b=a-b; // b=a+b-b -> b=a
    a=a-b; // a=a+b-a -> a=b
    printf("Csere összeadással és kivonással:\na=%d\nb=%d\n",a,b);
}
void xorswap(int a, int b)
{
    a=a^b;
    b=a^b;
    a=a^b;
    printf("Csere XOR-ral:\na=%d\nb=%d\n",a,b);

}