#include <stdio.h>
#include <math.h>

void print(double array[], int length, int it);
double distance (double rageRanks[], double prevPageRanks[], int length);
void calculate(double link[4][4]);
int main(){

    double link[4][4] = {           // Link mátrix
        {0.0,0.0,1.0/3.0,0.0},
        {1.0,1.0/2.0,1.0/3.0,1.0},
        {0.0,1.0/2.0,0.0,0.0},
        {0.0,0.0,1.0/3.0,0.0}
    };

    calculate(link);
}

void calculate(double link[4][4]){

    int it=0;
    double pageRanks[4] = {0.0,0.0,0.0,0.0};    // Új pagerank mátrix
    double prevPageRanks[4]={1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0}; // Kezdetleges / előző pagerank mátrix
    double const damping=0.0000000001;
    for(;;){

        for(int i=0;i<4;i++){
            pageRanks[i]=0.0;
            for(int j=0;j<4;j++)
            {
                pageRanks[i]=pageRanks[i]+link[i][j]*prevPageRanks[j]; // Pagerank kiszámítás
            }
        }

        if(distance(pageRanks,prevPageRanks,4)<damping){ // Ha az új pagerank és előző pagerank távolsága kisebb, mint a damping factor, leáll a program.
            break;
        }

        for (int i = 0; i < 4; i++)
        {
            prevPageRanks[i]=pageRanks[i]; // Ha nem kisebb az új PR lesz az előző.
        }
        it++;
        
    }
    print(pageRanks,4,it);

    }

double distance(double pageRanks[], double prevPageRanks[], int length){
    
    double distsum=0;
    for(int i=0;i<length;i++){

        distsum+=(pageRanks[i]-prevPageRanks[i])*(pageRanks[i]-prevPageRanks[i]); //Az új és előző pagerank távolsága
    }
    return sqrt(distsum);
}
void print(double array[],int length,int it){
    printf("A 4 honlap PageRank értéke (A-D): \n");
    for(int i=0;i<length;i++){

        printf("%f\n",array[i]);
    }
    printf("Iterációk száma: %d\n",it);
}
