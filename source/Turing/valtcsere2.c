#include <stdio.h>

void sumdivswap(int* a, int* b);
int main()
{
    int var1=12, var2=7;
    printf("Változók eredeti értéke:\na=%d\nb=%d\n",var1,var2);
    sumdivswap(&var1,&var2);
    printf("Változók cserélt értéke:\na=%d\nb=%d\n",var1,var2);
}
void sumdivswap(int* a, int* b)
{
    *a=*a+*b;
    *b=*a-*b; // b=a+b-b -> b=a
    *a=*a-*b; // a=a+b-a -> a=b
}