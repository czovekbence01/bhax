#include <iostream>

class Szulo{
    public:
    Szulo(){
        std::cout << "Szülő konstruktora" << std::endl;
    }

    virtual void uzenet(){
        std::cout << "Szülő üzenete" << std::endl;   
    }
};

class Gyerek : public Szulo{
    public:
    Gyerek(){
        std::cout << "Gyerek konstruktora"<< std::endl;
    }

    void uzenet(){
        std::cout << "Szülő üzenete felülírva"<< std::endl;
    }
    void uzenet2(){
        std::cout << "Gyerek üzenete"<< std::endl;
    }
};

int main(){

    Szulo* szptr = new Gyerek();

    szptr->uzenet();
    //szptr->uzenet2();

    return 0;
}