class Szulo{

    public Szulo(){
        System.out.println("Szülő konstruktora");
    }

    public void uzenet() {
        System.out.println("Szülő üzenet");
    }
}

class Gyerek extends Szulo{
    public Gyerek(){
        System.out.println("Gyerek konstruktora");
    }

    @Override
    public void uzenet(){
        System.out.println("Szülő üzenete felülírva");
    }

    public void uzenet2(){
        System.out.println("Gyerek üzenete");
    }
}

public class SzuloGyerek {

    public static void main(String[] args){

        Szulo obj = new Gyerek();

        obj.uzenet();
        //obj.uzenet2();

    }
}
