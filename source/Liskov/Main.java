package com.bence;

class Vehicle{

    public Vehicle(){
        System.out.println("Vehicle created");
    }

    public void start(){
        System.out.println("Vehicle started");
    }
}

class Car extends Vehicle{
    public Car(){
        System.out.println("Car created");
    }
    @Override
    public void start(){
        System.out.println("Car started");
    }
}

class Supercar extends Car{
    public Supercar(){
        System.out.println("Supercar created");
    }
    @Override
    public void start(){
        System.out.println("Supercar started");
    }
    public void test(){
        System.out.println("Teszt");
    }
}

public class Main {

    public static void main(String[] args) {

        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);
        Car secondVehicle = (Car) firstVehicle;
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);
        Supercar thirdVehicle = new Vehicle();
        thirdVehicle.start();
    }
}
