import java.util.Random;

class SortAndSearch{
    int n;
    Integer[] numbers;
    int storedDigits = 0;
    boolean sorted = false;

    public SortAndSearch(int n){
        this.n = n;
        this.numbers = new Integer[n];
    }

    public void add(int number){
        if(storedDigits<n) {
            numbers[storedDigits] = number;
            storedDigits++;
            sorted = false;
        }
        else {
            System.out.println("Limit reached. Number not added: "+number);
        }
    }

    public Integer[] sort(){
        if(!sorted) {
            for (int i = 0; i < storedDigits - 1; i++) {
                for (int j = 0; j < storedDigits - i - 1; j++) {
                    if (numbers[j] > numbers[j + 1]) {
                        int temp = numbers[j];
                        numbers[j] = numbers[j + 1];
                        numbers[j + 1] = temp;
                    }
                }
            }
            sorted = true;
        }
        return numbers;
    }

    public boolean isStored(int number){
        sort();
        return binarySearch(0 ,storedDigits,number);
    }

    private boolean binarySearch(int left, int right, int number){
        int mid = (left+right) / 2;

        if(right<left){
            return false;
        }
        if(number == numbers[mid]){
            return true;
        }else if(number < numbers[mid]){
            return binarySearch(left,mid-1,number);
        }else{
            return binarySearch(mid+1, right, number);
        }
    }
    public void print(){
        for (int i = 0; i<storedDigits; i++){
            System.out.print("{"+numbers[i]+"} ");
        }
        System.out.println();
    }
}

public class Bsbs {

    public static void main(String[] args){
        Random r = new Random();
        SortAndSearch ss = new SortAndSearch(10);
        ss.add(27);
        for (int i=0;i<10;i++){
            ss.add(r.nextInt(100));
        }
        ss.print();
        System.out.println(ss.isStored(27));
        ss.print();
        System.out.println(ss.isStored(65));
        Integer[] sortedArr = ss.sort();

    }
}
