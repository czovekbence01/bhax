import java.util.*;
import java.util.stream.Collectors;

class RandomClass{
    int f1;
    String f2;

    public RandomClass(int f1, String f2) {
        this.f1 = f1;
        this.f2 = f2;
    }
}

public class OOE {

    public static void main(String[] args) throws Exception {
        ArrayList<Integer> input1 = new ArrayList<>();
        input1.add(40);
        input1.add(30);
        input1.add(20);
        input1.add(10);

        List<Integer> actualOutput1 = createOrderedList(input1);

        for (Object o : actualOutput1){
            System.out.println(o);
        }

        HashSet<RandomClass> input2 = new HashSet<>();

        input2.add(new RandomClass(1,"a"));
        input2.add(new RandomClass(2,"b"));
        input2.add(new RandomClass(3,"c"));


        List<RandomClass> actualOutput2 = createOrderedList(input2);

        for (Object o : actualOutput2){
            System.out.println(o);
        }
    }

    public static <E> List<E> createOrderedList(Collection<E> input) throws Exception {
        try {
            return input.stream().sorted().collect(Collectors.toList());
        }
        catch(ClassCastException e){
            throw new Exception("syntax error");
        }
    }
}
