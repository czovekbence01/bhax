import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.util.Arrays;

public class Main extends Application {

    double screenWidth;
    double screenHeight;
    String[] table = new String[9];
    int turnCount = 1;
    boolean gameOver=false;

    public void setScreenSize(){
        Rectangle2D screen = Screen.getPrimary().getBounds();
        screenWidth = screen.getWidth();
        screenHeight = screen.getHeight();
    }

    public void drawTable(GraphicsContext context){
        context.setFill(Color.BLACK);
        context.fillRect(0,0 ,screenWidth, screenHeight);
        context.setFill(Color.WHITE);
        context.fillRect(screenWidth/3,0 ,20, screenHeight);
        context.fillRect(screenWidth-(screenWidth/3),0 ,20, screenHeight);
        context.fillRect(0 ,screenHeight/3.5,screenWidth,20);
        context.fillRect(0 ,screenHeight-(screenHeight/2.5),screenWidth,20);
    }

    public void drawChar(GraphicsContext context, double x, double y){
        context.setFont(new Font("",150));
        context.setFill(Color.WHITE);
        if(x<screenWidth/3){
            if(y<screenHeight/3.5 && table[0].isEmpty()){
                if((turnCount % 2) == 1)
                    table[0]="X";
                else
                    table[0]="O";
                context.fillText(table[0], (screenWidth/3)/2-75, (screenHeight/3.5)/2);
                turnCount++;
            }
            else if (y<screenHeight-(screenHeight/2.5) && table[3].isEmpty()){
                if((turnCount % 2) == 1)
                    table[3]="X";
                else
                    table[3]="O";
                context.fillText(table[3], (screenWidth/3)/2-75, screenHeight/2);
                turnCount++;
            }
            else{
                if((turnCount % 2) == 1)
                    table[6]="X";
                else
                    table[6]="O";
                context.fillText(table[6], (screenWidth/3)/2-75, screenHeight-(screenHeight/2.5)/2);
                turnCount++;
            }
        }
        else if(x<screenWidth-(screenWidth/3)){
            if(y<screenHeight/3.5 && table[1].isEmpty()){
                if((turnCount % 2) == 1)
                    table[1]="X";
                else
                    table[1]="O";
                context.fillText(table[1], screenWidth/2-50, (screenHeight/3.5)/2);
                turnCount++;
            }
            else if(y<screenHeight-(screenHeight/2.5) && table[4].isEmpty()){
                if((turnCount % 2) == 1)
                    table[4]="X";
                else
                    table[4]="O";
                context.fillText(table[4], screenWidth/2-50, screenHeight/2);
                turnCount++;
            }
            else{
                if((turnCount % 2) == 1)
                    table[7]="X";
                else
                    table[7]="O";
                context.fillText(table[7], screenWidth/2-50, screenHeight-(screenHeight/2.5)/2);
                turnCount++;
            }
        }
        else{
            if(y<screenHeight/3.5 && table[2].isEmpty()){
                if((turnCount % 2) == 1)
                    table[2]="X";
                else
                    table[2]="O";
                context.fillText(table[2], screenWidth-screenWidth/3+300, (screenHeight/3.5)/2);
                turnCount++;
            }
            else if(y<screenHeight-(screenHeight/2.5) && table[5].isEmpty()){
                if((turnCount % 2) == 1)
                    table[5]="X";
                else
                    table[5]="O";
                context.fillText(table[5], screenWidth-screenWidth/3+300, screenHeight/2);
                turnCount++;
            }
            else{
                if((turnCount % 2) == 1)
                    table[8]="X";
                else
                    table[8]="O";
                context.fillText(table[8], screenWidth-screenWidth/3+300, screenHeight-(screenHeight/2.5)/2);
                turnCount++;
            }
        }
        checkTable(context);
    }

    public void checkTable(GraphicsContext context){
        context.setFill(Color.RED);
        context.setFont(new Font("",100));
        for(int i=0;i<6;i+=3){
            if(table[i].equals(table[i+1]) && table[i].equals(table[i+2]) && !table[i].isEmpty() ) {
                gameOver = true;
                context.fillText(table[i]+" wins",screenWidth/2-200,screenHeight/2);
            }
        }
        for(int i=0;i<3;i++) {
            if (table[i].equals(table[i + 3]) && table[i].equals(table[i+6]) && !table[i].isEmpty()) {
                gameOver = true;
                context.fillText(table[i] + " wins", screenWidth / 2 - 200, screenHeight / 2);
            }
        }
        if(table[0].equals(table[4]) && table[0].equals(table[8]) && !table[0].isEmpty()){
            gameOver = true;
            context.fillText(table[4] + " wins", screenWidth / 2 - 200, screenHeight / 2);

        }
        if(table[2].equals(table[4]) && table[2].equals(table[6]) && !table[2].isEmpty()){
            gameOver = true;
            context.fillText(table[4] + " wins", screenWidth / 2 - 200, screenHeight / 2);

        }

        if(turnCount>9 && !gameOver){
            gameOver = true;
            context.fillText("Tie", screenWidth / 2 - 100, screenHeight / 2);
        }

    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Arrays.fill(table,"");
        setScreenSize();
        Canvas c = new Canvas(screenWidth,screenHeight);
        GraphicsContext context = c.getGraphicsContext2D();
        primaryStage.setScene(new Scene(new Pane(c)));
        primaryStage.setFullScreen(true);
        primaryStage.show();
        drawTable(context);
        c.setOnMouseClicked(mouseEvent -> {
            if(!gameOver) {
                drawChar(context, mouseEvent.getX(), mouseEvent.getY());
            }
            else{
                primaryStage.close();
            }
        });
    }


    public static void main(String[] args) {
        launch(args);
    }
}
