package com.bence;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class ASCIIArt {

    private BufferedImage img;
    private String outputFileName;
    private final char[] lightChars = {'.',',', '-', '~'};
    private final char[] darkChars = {'@','#', '&', 'X'};
    private Random rand = new Random();

    public ASCIIArt(BufferedImage img, String outputFileName) {
        this.img = img;
        this.outputFileName = outputFileName;
    }

    public void create() throws IOException {
        drawASCIIArt(img);
    }

    private void drawASCIIArt(BufferedImage img) throws IOException {
        FileOutputStream output = new FileOutputStream(outputFileName);
        for(int i = 0; i< img.getHeight(); i++){

            for(int j = 0; j<img.getWidth(); j++){
                Color c =  new Color(img.getRGB(j,i));
                int red = (int)(c.getRed()*0.299);
                int green = (int)(c.getGreen()*0.587);
                int blue = (int) (c.getBlue()*0.114);
                int gray = red + green + blue;
                //System.out.println(gray);
                if(gray<100)
                    output.write(darkChars[rand.nextInt(3)]);
                else if(gray<230)
                    output.write(lightChars[rand.nextInt(3)]);
                else
                    output.write(' ');
            }
            output.write('\n');
        }
    }
}
