package com.bence;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        String imgName = args[0];
        String outputName = args.length > 1 ? args[1] : "output.txt";
        if(!imgName.isEmpty()){
            BufferedImage img = ImageIO.read(new File(imgName));
            ASCIIArt a = new ASCIIArt(img, outputName);

            a.create();
        }
        else {
            System.err.println("Image not found");
        }
}
}
