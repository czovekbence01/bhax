package com.bence;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CaesarEncoder {
    private final FileOutputStream output;

    public CaesarEncoder(String outputName) throws FileNotFoundException {
        this.output = new FileOutputStream(outputName);
    }

    public void encode(String s, int offset) throws IOException {

        for(char character : s.toCharArray()){
            if(character != ' ')
            {
                char newChar = (char)((character + offset) % 128);
                output.write(newChar);
            }
            else {
                output.write(character);
            }
        }
        output.write('\n');
    }
}
