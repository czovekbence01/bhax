package com.bence;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {

        String outputFileName = args.length > 0 ? args[0] : "output.txt";
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s;
        CaesarEncoder c = new CaesarEncoder(outputFileName);
        while(true){
            s = reader.readLine();
            if(s.isEmpty())
                break;
            c.encode(s,5);
        }
    }
}
