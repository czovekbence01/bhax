class ParentException extends Exception {
    public ParentException() {
    }
}

class ChildException extends ParentException{
    public ChildException() {
    }
}
