package com.bczovek.BinfaAOP;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public aspect Logger {
	
	pointcut insertLogger(LZWBinfa fa, char b): execution(void LZWBinfa.insert(char)) && target(fa) && args(b);
	pointcut buildTreeLogger(String input): call(void LZWBinfa.buildTree(String)) && args(input);
	pointcut clearLogFile(): execution(void Main.main(String[]));
	
	before(String input) : buildTreeLogger(input){
		try (PrintWriter os = new PrintWriter(new FileWriter("log.txt", true))){
		os.println("Building tree with input: "+input);
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	after(String input): buildTreeLogger(input){
		try (PrintWriter os = new PrintWriter(new FileWriter("log.txt", true))){
		os.println("Building tree finished");
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	after(LZWBinfa fa, char b) : insertLogger(fa, b){
		try (PrintWriter os = new PrintWriter(new FileWriter("log.txt", true))){
		os.println("Inserted: "+ b);
		os.println("New tree:");
		fa.print(os);
		os.println("depth: "+fa.getDepth());
        os.println("mean: "+fa.getAvg());
        os.println("var: "+ fa.getDeviation());
		os.println("-----");
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	before() : clearLogFile(){
		try (PrintWriter os = new PrintWriter(new FileWriter("log.txt"))){
			os.println("");
			}
			catch(IOException e) {
				e.printStackTrace();
			}
	}
	
}
