package com.bczovek.BinfaAOP;

import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		PrintWriter os = new PrintWriter(System.out, true);
		String input = "0100101";
		
		LZWBinfa fa = new LZWBinfa();
		fa.buildTree(input);
		fa.print(os);
		os.println("depth: "+fa.getDepth());
        os.println("mean: "+fa.getAvg());
        os.println("var: "+ fa.getDeviation());
	}

}
