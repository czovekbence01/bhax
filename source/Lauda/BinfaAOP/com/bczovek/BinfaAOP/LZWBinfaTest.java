package com.bczovek.BinfaAOP;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LZWBinfaTest {
	
	private LZWBinfa testTree;
	private final String testInput = "01111001001001000111";
	private final double expectedAvg = 2.75;
	private final int expectedDepth = 4;
	private final double expectedDeviation = 0.9574;
	
	void buildTestTree(){
		if(testTree == null) {
			testTree = new LZWBinfa();
			testTree.buildTree(testInput);
		}
	}
	
	@Test
	void testAvg() {
		buildTestTree();
		assertEquals(expectedAvg, testTree.getAvg(),0);
	}

	@Test
	void testDepth() {
		buildTestTree();
		assertEquals(expectedDepth, testTree.getDepth(), 0);
	}
	
	@Test
	void testDeviation() {
		buildTestTree();
		assertEquals(expectedDeviation, testTree.getDeviation(), 0.0001);
	}
}
