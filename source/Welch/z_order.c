// z.c
//
// LZW fa építő
// Programozó Páternoszter
//
// Copyright (C) 2011, Bátfai Norbert, nbatfai@inf.unideb.hu, nbatfai@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Ez a program szabad szoftver; terjeszthetõ illetve módosítható a
// Free Software Foundation által kiadott GNU General Public License
// dokumentumában leírtak; akár a licenc 3-as, akár (tetszõleges) késõbbi
// változata szerint.
//
// Ez a program abban a reményben kerül közreadásra, hogy hasznos lesz,
// de minden egyéb GARANCIA NÉLKÜL, az ELADHATÓSÁGRA vagy VALAMELY CÉLRA
// VALÓ ALKALMAZHATÓSÁGRA való származtatott garanciát is beleértve.
// További részleteket a GNU General Public License tartalmaz.
//
// A felhasználónak a programmal együtt meg kell kapnia a GNU General
// Public License egy példányát; ha mégsem kapta meg, akkor
// tekintse meg a <http://www.gnu.org/licenses/> oldalon.
//
//
// Version history:
//
// 0.0.1, http://progpater.blog.hu/2011/02/19/gyonyor_a_tomor
// 0.0.2, csomópontok mutatóinak NULLázása (nem fejtette meg senki :)
// 0.0.3, http://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct binfa
{
  int ertek;
  struct binfa *bal_nulla;
  struct binfa *jobb_egy;

} BINFA, *BINFA_PTR;

BINFA_PTR
uj_elem ()
{
  BINFA_PTR p;

  if ((p = (BINFA_PTR) malloc (sizeof (BINFA))) == NULL)
    {
      perror ("memoria");
      exit (EXIT_FAILURE);
    }
  return p;
}

void kiir (BINFA_PTR elem, int order);
void szabadit (BINFA_PTR elem);
int max_melyseg = 0, melyseg = 0;

int
main ()
{
  char b;

  BINFA_PTR gyoker = uj_elem ();
  gyoker->ertek = '/';
  gyoker->bal_nulla = gyoker->jobb_egy = NULL;
  BINFA_PTR fa = gyoker;
while (read (0, (void *) &b, 1))
    {
      if (b == '0')
	{
	  if (fa->bal_nulla == NULL)
	    {
	      fa->bal_nulla = uj_elem ();
	      fa->bal_nulla->ertek = 0;
	      fa->bal_nulla->bal_nulla = fa->bal_nulla->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
	      fa = fa->bal_nulla;
	    }
	}
      else if (b == '1')
	{
	  if (fa->jobb_egy == NULL)
	    {
	      fa->jobb_egy = uj_elem ();
	      fa->jobb_egy->ertek = 1;
	      fa->jobb_egy->bal_nulla = fa->jobb_egy->jobb_egy = NULL;
	      fa = gyoker;
	    }
	  else
	    {
	      fa = fa->jobb_egy;
	    }
	}
    }

    printf ("\n");
    printf ("Inorder:\n");
    kiir (gyoker,-1);
    printf ("\n");
    
    printf ("Preorder:\n");
    kiir (gyoker,0);
    printf ("\n");

    printf ("Postorder:\n");
    kiir (gyoker,1);


  printf ("melyseg=%d\n", max_melyseg - 1);

  szabadit (gyoker);
}

void
kiir (BINFA_PTR elem, int order)
{
    switch(order){  
        case 0:
            if (elem != NULL)
            {
            ++melyseg;
            if (melyseg > max_melyseg)
                max_melyseg = melyseg;
            
            for (int i = 0; i < melyseg; ++i)
                printf ("---");
            printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek,
                melyseg - 1); 
            kiir (elem->bal_nulla, order);
            kiir (elem->jobb_egy, order);     
            --melyseg;
            }
            break;
        case 1:
            if (elem != NULL)
            {
            ++melyseg;
            if (melyseg > max_melyseg)
                max_melyseg = melyseg;
            
                kiir (elem->bal_nulla, order);
                kiir (elem->jobb_egy, order); 
                for (int i = 0; i < melyseg; ++i)
                printf ("---");
                printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek, melyseg - 1);     
                --melyseg;
            }
            break;
        default:
            if (elem != NULL)
                {
                ++melyseg;
                if (melyseg > max_melyseg)
                    max_melyseg = melyseg;
                
                    kiir (elem->bal_nulla, order);
                    for (int i = 0; i < melyseg; ++i)
                    printf ("---");
                    printf ("%c(%d)\n", elem->ertek < 2 ? '0' + elem->ertek : elem->ertek, melyseg - 1);     
                    kiir (elem->jobb_egy, order); 
                    --melyseg;
                    
                }
            break;
                
    }
}

void
szabadit (BINFA_PTR elem)
{
  if (elem != NULL)
    {
      szabadit (elem->jobb_egy);
      szabadit (elem->bal_nulla);
      free (elem);
    }
}
