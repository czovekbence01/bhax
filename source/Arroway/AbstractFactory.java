import java.util.Scanner;

interface Button{
    void paint();
}

interface Checkbox{
    void paint();
}
class MacOSButton implements Button{
    @Override
    public void paint(){
        System.out.println("MacOS button created.");
    }
}
class WinButton implements Button{
    @Override
    public void paint(){
        System.out.println("Windows button created.");
    }
}
class MacOSCheckbox implements Checkbox{
    @Override
    public void paint(){
        System.out.println("MacOS checkbox created.");
    }
}
class WinCheckbox implements Checkbox{
    @Override
    public void paint(){
        System.out.println("Windows checkbox created.");
    }
}
interface GUIFactory{
    Button createButton();
    Checkbox createCheckbox();
}

class MacOSFactory implements GUIFactory{
    @Override
    public Button createButton(){
        return new MacOSButton();
    }
    @Override
    public Checkbox createCheckbox(){
        return new MacOSCheckbox();
    }
}
class WinFactory implements GUIFactory{
    @Override
    public Button createButton(){
        return new WinButton();
    }
    @Override
    public Checkbox createCheckbox(){
        return new WinCheckbox();
    }
}
class Application{
    private Button button;
    private Checkbox checkbox;

    public Application(GUIFactory factory){
        button = factory.createButton();
        checkbox = factory.createCheckbox();
    }
    public void paint(){
        button.paint();
        checkbox.paint();
    }
}
public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter your os name: (Mac/Win)");
        GUIFactory factory=null;
        Application app=null;
        if(userInput.hasNextLine()){
            if(userInput.nextLine().toLowerCase().equals("mac")){
                factory = new MacOSFactory();
                app = new Application(factory);
            }else if (userInput.nextLine().toLowerCase().equals("win")){
                factory = new WinFactory();
                app = new Application(factory);
            }
        }
        app.paint();
    }
}