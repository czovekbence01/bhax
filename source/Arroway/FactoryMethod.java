import java.util.Scanner;

abstract class Subscription {
    private int monthlyPrice;
    private int amountofMonths;

    public int getMonthlyPrice() {
        return monthlyPrice;
    }
    public void setMonthlyPrice(int monthlyPrice) {
        this.monthlyPrice = monthlyPrice;
    }
    public int getAmountofMonths() {
        return amountofMonths;
    }
    public void setAmountofMonths(int amountofMonths) {
        this.amountofMonths = amountofMonths;
    }
    public int calcTotalPrice(){
        return monthlyPrice*amountofMonths;
    }
}

class NormalSubscription extends Subscription{
    public NormalSubscription(){
        setAmountofMonths(3);
        setMonthlyPrice(5);
    }
}

class PremiumSubscription extends Subscription{
    public PremiumSubscription(){
        setAmountofMonths(6);
        setMonthlyPrice(4);
    }
}

class SubscriptionFactory{
    public Subscription newSubscription(String subscriptionType){

        switch(subscriptionType){
            case "Normal":
                return new NormalSubscription();
            case "Premium":
                return new PremiumSubscription();
            default:
                return null;
        }
    }
}

public class Main{

    public static void main(String[] args) {
	    SubscriptionFactory factory=new SubscriptionFactory();

	    Subscription sub = null;
        Scanner userInput = new Scanner(System.in);
        System.out.println("Choose a subscription!");
        System.out.println("Normal/Premium");
        if(userInput.hasNextLine()){
            sub = factory.newSubscription(userInput.nextLine());
        }

        if(sub != null){
            System.out.println("Price: $" + sub.calcTotalPrice());
        }
    }
}
