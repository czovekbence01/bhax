class Singleton{
    private static Singleton firstInstance = null;

    private Singleton(){}

    public static Singleton getInstance(){

        if(firstInstance == null){
            firstInstance = new Singleton();
        }
        return firstInstance;
    }

}

public class SingletonDemo {
    public static void main(String args[]){
        //Singleton test = new Singleton(); nem működik mivel a konstruktor private.
        Singleton test = Singleton.getInstance();
        System.out.println(test);
        Singleton test2 = Singleton.getInstance();
        System.out.println(test2); // test és test2 megegyezik.
    }
}