import java.io.*;

public class LZWBinfa {
	
	private Node root;
    private int maxDepth;
    private double avg;
    private double deviation;
    private Node pointer;
    private int depth;
    private int avgSum;
    private int avgCount;
    private double deviationSum;

    private class Node {


        private char rootChar;
        private Node leftChild;
        private Node rightChild;

        public Node() {
            this('/');
        }

        public Node(char b) {

            rootChar = b;
            leftChild = null;
            rightChild = null;

        }


        public Node getLeftChild() {

            return leftChild;
        }

        public Node getRightChild() {

            return rightChild;
        }

        public void newLeftChild(Node zeroNode) {

            leftChild = zeroNode;
        }

        public void newRightChild(Node oneNode) {

            rightChild = oneNode;
        }

        public char getRootChar() {

            return rootChar;
        }
    }

    public LZWBinfa() {
        root = new Node();
        pointer = root;
    }

    public void insert(char b) {

        if(b == '0') {
            if(pointer.getLeftChild() == null) {
                pointer.newLeftChild(new Node('0'));
                pointer = root;
            }
            else {
                pointer = pointer.getLeftChild();
            }
        }
        else {

            if(pointer.getRightChild() == null) {
                pointer.newRightChild(new Node('1'));
                pointer = root;
            }
            else {
                pointer = pointer.getRightChild();
            }
        }
    }

    public void buildTree(String input) {

        for(int i = 0; i<input.length(); i++) {
            insert(input.charAt(i));
        }

    }

    public void print(PrintWriter os){

        recursivePrint(this.root, os);
        depth = 0;
    }

    private void recursivePrint(Node root, PrintWriter os){
        if(root != null) {
            ++depth;
            recursivePrint(root.getLeftChild(), os);
            for(int i = 0; i<depth; i++){
                os.print("---");
            }
            os.println(root.getRootChar()+" ("+ (depth-1) +") ");
            recursivePrint(root.getRightChild(),os);
            --depth;
        }

    }

    private void rdepth(Node root){

        if(root != null){
            ++depth;
            if(depth > maxDepth){
                maxDepth = depth;
            }
            rdepth(root.getLeftChild());
            rdepth(root.getRightChild());
            --depth;
        }

    }

    private void ravg(Node root){

        if(root != null){
            ++depth;
            ravg(root.getLeftChild());
            ravg(root.getRightChild());
            --depth;
            if(root.getRightChild() == null && root.getLeftChild() == null){
                ++avgCount;
                avgSum += depth;
            }
        }

    }

    private void rdeviation(Node root){

        if(root != null){
            ++depth;
            rdeviation(root.getLeftChild());
            rdeviation(root.getRightChild());
            --depth;
            if(root.getRightChild() == null && root.getLeftChild() == null){
                ++avgCount;
                deviationSum += (depth - avg)*(depth - avg);
            }
        }

    }

    public int getDepth(){
        depth = maxDepth = 0;
        rdepth(root);
        return maxDepth-1;
    }

    public double getAvg(){
        depth = avgSum = avgCount = 0;
        ravg(root);
        avg = (double)avgSum / avgCount;
        return avg;
    }
    
    public double getDeviation(){
        avg = getAvg();
        deviationSum = 0.0;
        depth = avgCount = 0;
        rdeviation(root);
        if(avgCount-1 > 0){
           deviation = Math.sqrt(deviationSum / (avgCount -1 ));
        }else{
            deviation = Math.sqrt(deviationSum);
        }
        return deviation;
    }

}
