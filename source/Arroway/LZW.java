import java.io.*;
import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LZW")
public class LZW extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter os = response.getWriter();
		String input = request.getParameter("input");
		if(input != null) {
			LZWBinfa fa = new LZWBinfa();
			fa.buildTree(input);
			fa.print(os);
			os.println("depth: "+fa.getDepth());
	        os.println("mean: "+fa.getAvg());
	        os.println("var: "+ fa.getDeviation());
	        
		}else {
			os.println("/LZWServlet/LZW?input=<input>");
		}
	}


}
