import java.util.Objects;

class Customer{
    private int id;
    private String name;

    public Customer(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Customer customer = (Customer) obj;
        return id == customer.id &&
                name.equals(customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}

public class Main {

    public static void main(String[] args) {
        Customer customer1 = new Customer(1,"Bence");
        Customer customer2 = new Customer(1,"Bence");

        System.out.println(customer1.equals(customer2));
    }
}
