interface CarPlan{
    void setEngine(String engine);
    void setTransmission(String transmission);
    void setSeats(String seats);
    void setWheelSize(String wheelSize);
}
class Car implements CarPlan{
    private String engine;
    private String transmission;
    private String seats;
    private String wheelSize;
    @Override
    public void setEngine(String engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setSeats(String seats) {
        this.seats = seats;
    }

    @Override
    public void setWheelSize(String wheelSize) {
        this.wheelSize = wheelSize;
    }

    public String getEngine() {
        return engine;
    }

    public String getTransmission() {
        return transmission;
    }

    public String getSeats() {
        return seats;
    }

    public String getWheelSize() {
        return wheelSize;
    }
}
interface CarBuilder{
    void buildEngine();
    void buildTransmission();
    void buildSeats();
    void buildWheels();
    Car getCar();
}
class SportcarBuilder implements CarBuilder{

    private Car car;

    public SportcarBuilder(){
        this.car = new Car();
    }
    @Override
    public void buildEngine() {
        car.setEngine("V8");
    }

    @Override
    public void buildTransmission() {
        car.setTransmission("Automatic");
    }

    @Override
    public void buildSeats() {
        car.setSeats("2");
    }

    @Override
    public void buildWheels() {
        car.setWheelSize("16\"");
    }
    @Override
    public Car getCar(){
        return this.car;
    }
}
class Director{
    private CarBuilder carBuilder;

    public Director(CarBuilder carBuilder){
        this.carBuilder = carBuilder;
    }
    public Car getCar(){
        return this.carBuilder.getCar();
    }
    public void buildCar(){
        this.carBuilder.buildEngine();
        this.carBuilder.buildSeats();
        this.carBuilder.buildTransmission();
        this.carBuilder.buildWheels();
    }
}
public class BuilderDemo {

    public static void main(String args[]){
        CarBuilder sportCar = new SportcarBuilder();
        Director director = new Director(sportCar);

        director.buildCar();
        Car car1 = director.getCar();
        System.out.println(car1.getEngine());
        System.out.println(car1.getTransmission());
        System.out.println(car1.getSeats());
        System.out.println(car1.getWheelSize());
    }
}
