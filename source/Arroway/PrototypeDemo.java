interface Animal extends Cloneable{
    public Animal makeCopy();
}
class Sheep implements Animal{
    public Sheep(){
        System.out.println("Sheep is made");
    }
    @Override
    public Animal makeCopy() {
        System.out.println("Sheep is being cloned");

        Sheep sheep1 = null;
        try {
            sheep1 = (Sheep) super.clone();

        }catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        return sheep1;
    }

}

public class PrototypeDemo {
    public static void main(String args[]){

        Sheep sheep = new Sheep();
        Sheep clonedSheep = (Sheep) sheep.makeCopy();
    }
}
