public class KOE {

    public static void main(String[] args) {

        // Given
        String first = "...";
        String second = "...";
        String third = new String("...");
        System.out.println(System.identityHashCode(first) + " " +System.identityHashCode(second) + " " +System.identityHashCode(third));
        System.out.println(first.hashCode()+" "+ second.hashCode()+" "+third.hashCode());
        // When
        var firstMatchesSecondWithEquals = first.equals(second);
        var firstMatchesSecondWithEqualToOperator = first == second;
        var firstMatchesThirdWithEquals = first.equals(third);
        var firstMatchesThirdWithEqualToOperator = first == third;

        System.out.println(firstMatchesSecondWithEquals);
        System.out.println(firstMatchesSecondWithEqualToOperator);
        System.out.println(firstMatchesThirdWithEquals);
        System.out.println(firstMatchesThirdWithEqualToOperator);
    }
}